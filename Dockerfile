# Build stage
FROM rust:1.69-buster as builder

WORKDIR /rocketwebsite

# Accept the build argument
ARG DATABASE_URL

# Make sure to use the ARG in ENV
ENV DATABASE_URL=$DATABASE_URL

# Copy the source code
COPY . .

RUN pwd
# Build the application
RUN rustup default 1.7.0
RUN cargo build --release


# Production stage
FROM debian:buster-slim
WORKDIR /usr/local/bin
COPY --from=builder /rocketwebsite/target/release/rocketwebsite .
COPY --from=builder /rocketwebsite/sql .
CMD ["./rocketwebsite"]
